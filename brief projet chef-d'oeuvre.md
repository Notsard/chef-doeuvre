#Projet chef-d'oeuvre.
====================
1. Competance visée :

- Maquetter une application : transposer (minimum 80%)

- Realiser une interface web statique et adaptable : transposer

- Developper une interface utilisateur web dynamique : transposer (minimum 70%)

- Créer une base de donnée : adapter (minimum 50%)

- Developper les composants d'acces au données : adapter (minimum 60%)

- Devolopper une partie back-end d'une application web ou web mobile : transposer (minimum 40%)
------------------------------------------------------------------------------------------------
2. Desciption du projet :
Créer un site pour le photographe de pont de beauvoisin avec les fonctionnalité suivante : avoir un espace client avec un drive pour partagée les photo prise pas le photographe pour qu'il puisse les envoyé aux clients sans qu'il se deplace comme un google drive. Chaque client peuvent mettre une photo en .jpg ou .png mais ne peuvent pas voir les photo des autres clients. Le photographe (admin du site) peut voir toutes les photo et les recupérer pour les modifié et les renvoyé au client dedié.
-----------------------------------------------------------------------------------------------------------------------------------------------
3. Les techno utilisé :
Je vais utilisé symfony pour la gestion de la base de donnée et la gestion des utilisateur, peut etre integre react au projet pour utilise API google drive mais pas sur.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4. Semaines de travail :
## Semaine 1 : travail préparatoire :
* un diagramme des classes pour représenter l'ensemble des entités du projet
* un schéma uml des use case
* les wireframes pour chaque cas d'utilisation

## Semaine 2 :
Commencer a coder sur symfony
## Semaine 3 :

## Semaine 4 :

## Semaine 5 :
Finalisation

