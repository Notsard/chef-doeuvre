<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Photo;
use App\Form\ImageType;
use App\Entity\Categories;
use App\Form\AddPhotoType;
use App\Form\RechercheType;
use App\Form\CategoriesType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
    /**
     * @Route("/admin/galerie/add", name="add")
     */
    public function add(Request $request, ObjectManager $Manager){
        $photo = new Photo();

        $form = $this->createForm(AddPhotoType::class, $photo);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){
                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $Manager->persist($photo);
                $Manager->flush();
                return $this->redirectToRoute('admin');
            }
        return $this->render('galerie/add.html.twig',[
                'formGalery' => $form->createView(),
                ]);
    }

    /**
     * @Route("/admin/delete/image/{id}", name="delete_image")
     */
    public function deleteImage($id)
    {

        $repository = $this->getDoctrine()->getRepository(Photo::class);
        $imageToDelete = $repository->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($imageToDelete);
        $entityManager->flush();
        return $this->redirectToRoute('galerie');
    }

    /**
     * @Route("/admin/categorie/add", name="add_categories")
     */
    public function addCategorie(Request $request, ObjectManager $Manager){
        $categorie = new Categories();

        $form = $this->createForm(CategoriesType::class, $categorie);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){
                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $Manager->persist($categorie);
                $Manager->flush();
                return $this->redirectToRoute('admin');
            }
        return $this->render('categories/add.html.twig',[
                'formCategorie' => $form->createView(),
                ]);
    }

    /**
     * @Route("/admin/user/liste", name="user_list")
     */
    public function Users(Request $request, PaginatorInterface $paginator){
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->findAll();
        $pagination = $paginator->paginate(
        $users,
        $request->query->getInt('page', 1),
        4
    );

        return $this->render('user/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
    /**
     * @Route("/admin/user/{id}", name="user_view")
     */
    public function userView($id, Request $request, ObjectManager $Manager)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $images = $this->getDoctrine()->getRepository(Photo::class)->findAll();

        return $this->render('admin/userview.html.twig', [
            'images' => $images,
            'id' => $user,
        ]);
    }

    /**
     * @Route("/admin/user/edit/{id}", name="photo_edit")
     */
    public function edit(Request $request, $id)
    {
        $photo = $this->getDoctrine()->getRepository(Photo::class)->find($id);
        $user = $this->getDoctrine()->getREpository(User::class)->find($id);
        $images = $this->getDoctrine()->getRepository(Photo::class)->findAll();
        $form = $this->createForm(ImageType::class, $photo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute('admin');
        }

        return $this->render('user/edit.html.twig', [
            'photo' => $photo,
            'form' => $form->createView(),
            'user' => $user,
            'image' => $images,
        ]);
    }

    /**
     * @Route("/admin/gestion/user", name="user_gestion")
     */
    public function gestionUser(Request $request, PaginatorInterface $paginator): Response
    {

        $form = $this->createForm(RechercheType::class);
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->findAll();
        $pagination = $paginator->paginate (
            $users,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('admin/gestion.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'users' => $users,
        ]);
    }
}
