<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Image;
use App\Entity\Photo;
use App\Entity\Message;
use App\Form\ImageType;
use App\Form\MessageType;
use App\Entity\Categories;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/galerie", name="galerie")
     */
    public function galerie(){
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Categories::class)->findAll();
        return $this->render('galerie/home.html.twig',[
            'categories' => $entities,
        ]);
    }

    /**
     * @Route("/galerie/{id}", name="categories")
     */
    public function galery($id){
        $photos = $this->getDoctrine()->getRepository(Photo::class)->findImagesFromCategory($id);
        $categorie = $this->getDoctrine()->getRepository(Categories::class)->findOneBy([
            'id' => $id,
        ]);
        return $this->render('galerie/photo.html.twig', [
            'photos' => $photos,
            'categorie' => $categorie
        ]);
    }

    /**
     * @Route("/image/add", name="add_image")
     */
    public function ImageForm(Request $request, ObjectManager $Manager){
        $photo = new Photo();

        $form = $this->createForm(ImageType::class, $photo);

        $user = $this->getUser();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){
                $photo->setUser($user);
                $Manager->persist($photo);
                $Manager->flush();
                return $this->redirectToRoute('home');
            }
        return $this->render('image/add.html.twig',[
                'formImage' => $form->createView(),
                ]);
    }

    /**
     * @Route("/image/view/", name="image_view")
     */
    public function image()
    {

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(Photo::class);
        $user = $this->getUser();

        return $this->render('image/view.html.twig',[
            'images' => $entities->findAll(),
            'users' => $user,
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {

        return $this->render('contact/home.html.twig');
    }
    

}
