<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Faker;

class UsersFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for($i=0; $i<200; $i++){
        $product = new User();
        $product->setUsername($faker->name);
        $product->setEmail($faker->email);
        $product->setEnabled(1);
        $product->setSalt(null);
        $product->setPassword($faker->password);
        $product->setFirstname($faker->firstName);
        $manager->persist($product);

    }
        $manager->flush();
    }
}
