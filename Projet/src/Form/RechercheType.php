<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SearchType;

class RechercheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', SearchType::class, [
                'required' => false,
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add('email', SearchType::class, [
                'required' => false,
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add('firstname', SearchType::class, [
              'required' => false,
              'attr' => [
                'class' => "form-control",
              ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
